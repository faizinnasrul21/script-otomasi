# script-otomasi

Automation Script Tasks (Harry & Nasrul).
This script helps you to install Apache2 and MySQL with just a single command.


## INSTALL
To install, simply download the script file and give it the executable permission.
```
curl -0 https://gitlab.com/faizinnasrul21/script-otomasi/-/raw/main/script_install.sh
chmod +x script-install.sh
```

## USAGE
```
sudo ./script-install.sh
```

## OUTPUT SAMPLE
![](./screenshoot-install-1.jpg)
![](./screenshoot-install-2.jpg)
